(function(){
  'use strict';
  
  function Runner() {
    _ = undefined;
  }
  
  function Factory(origin) {
    return origin;
  }
  
  angular
    .module('angularLodashIsolated', [])
    .constant('origin', _.cloneDeep(_))
    .run(Runner)
    .factory('_', ['origin', Factory]);

});
